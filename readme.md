using pylon version 4


flight radar adsb
-----------------

index:
  - 0: registration hex (string)
  - 1: latitude (double)
  - 2: longitude (double)
  - 3: heading (int)
  - 4: altitude (int)
  - 5: speed (int)
  - 6: squawk (int)
  - 7: radar (string)
  - 8: aircraft type (string)
  - 9: registration (string)
  - 10: ?
  - 11: airport from (string)
  - 12: airport to (string)
  - 13: flight id (string)
  - 14: ?
  - 15: vertical speed (int)
  - 16: callsign (string)
  - 17: ?
